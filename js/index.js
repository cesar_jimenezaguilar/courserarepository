$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 100
    });

    $("#contact").on("show.bs.modal", function (e) {
        console.log("el modal se esta mostrando");
        $('#contactBtn').removeClass('btn-outline-secondary');
        $('#contactBtn').addClass('btn-primary');
        $('#contactBtn').prop('disabled', true);
    });
    $("#contact").on("hide.bs.modal", function (e) {
        console.log("el modal se ocultó");
        $('#contactBtn').prop('disabled', false);
        $('#contactBtn').removeClass('btn-primary');
        $('#contactBtn').addClass('btn-outline-secondary');
    });

});